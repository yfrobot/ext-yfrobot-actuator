/*
   legoduinoPro_Fan.h
    - legoduinoPro 主板 MA MB端口专用库程序

   Author     : YFROBOT ZL
   Website    : www.yfrobot.com.cn
   Create Time: 2024-06-18
   Copyright (c) 2024 YFROBOT
   Change Log :

   The MIT License (MIT)

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

#ifndef LegoduinoPro_Fan_h
#define LegoduinoPro_Fan_h
#include <Arduino.h>


#define MAPinDir 4
#define MAPinSpeed 5
#define MBPinDir 7
#define MBPinSpeed 6

/*
  驱动电机函数
  参数：mASpeed - 左电机速度 （取值范围：-255 ~ 255）
*/
void legoduinoProSetAMotor(int mASpeed) {
  if (mASpeed > 0) {
    digitalWrite(MAPinDir, LOW);
    analogWrite(MAPinSpeed, mASpeed);
  } else if (mASpeed < 0) {
    digitalWrite(MAPinDir, HIGH);
    analogWrite(MAPinSpeed, abs(mASpeed));
  } else {
    analogWrite(MAPinSpeed, mASpeed);
  }
}

/*
  驱动电机函数
  参数：mBSpeed - 右电机速度 （取值范围：-255 ~ 255）
*/
void legoduinoProSetBMotor(int mBSpeed) {
  if (mBSpeed > 0) {
    digitalWrite(MBPinDir, LOW);
    analogWrite(MBPinSpeed, mBSpeed);
  } else if (mBSpeed < 0) {
    digitalWrite(MBPinDir, HIGH);
    analogWrite(MBPinSpeed, abs(mBSpeed));
  } else {
    analogWrite(MBPinSpeed, mBSpeed);
  }
}

#endif
